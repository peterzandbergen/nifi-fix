#!/bin/bash

BASE=$(dirname $0)
CMD=$BASE/../nififix

"nififix",
		"fetch-gcs-object",
		"--bucket-attr=bucket",
		"--object-attr=object",
		"--out=/tmp/test-run-app.out",
		"--credentials-file=../secrets/blackops-upload-download-d6aac693fc6b.json",
		"--attributes", `{
			"bucket":"magweg",
			"object": "upload download mappings.xlsx"
		}`,

$CMD $*
