package main

import (
	"bytes"
	"context"
	"nififix"
	"nififix/gcp"
	"testing"

	"github.com/go-logr/logr"
)

const (
	attrsInput = `{"key": "val"}`
)

func TestInterface(t *testing.T) {
	attrs, err := nififix.PrepareAttributes([]byte(attrsInput))
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	in := bytes.NewBufferString("hallo from bucket")
	out := &bytes.Buffer{}

	pctx := &nififix.ProcessorContext{
		In:         in,
		Out:        out,
		Attributes: attrs,
		Logger:     logr.Discard(),
	}
	ctx := context.Background()
	fp := &gcp.FetchGCSObjectProcessor{}
	fp.Process(ctx, pctx)
}
