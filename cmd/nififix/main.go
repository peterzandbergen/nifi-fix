package main

import (
	"context"
	"fmt"
	"nififix/actions"

	"os"
)

func run(args []string) error {
	return actions.NewApp().RunContext(context.Background(), args)
}

func main() {
	if err := run(os.Args); err != nil {
		fmt.Fprintf(os.Stderr, "error: %s\n", err)
		os.Exit(1)
	}
	os.Exit(0)
}
