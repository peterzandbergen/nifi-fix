package gcp

import (
	"context"
	"io"
	"nififix"

	"cloud.google.com/go/storage"
	"google.golang.org/api/option"
)

type GCPContext struct {
	CredentialsFile string
}

func (c *GCPContext) NewStorageClient(ctx context.Context) (*storage.Client, error) {
	sc, err := storage.NewClient(ctx, option.WithCredentialsFile(c.CredentialsFile))
	if err != nil {
		return nil, err
	}
	return sc, nil
}

type FetchGCSObjectProcessorContext struct {
	*nififix.ProcessorContext

	Bucket          string
	Object          string
	CredentialsFile string
}

// NewFetchGCSObjectProcessorContext creates a new context from the processor context.
//
// Extract the bucket and the object name from the attributes.
// Extract the Credentials file from the attributes.
// Return the initialized context.
func NewFetchGCSObjectProcessorContext(pctx *nififix.ProcessorContext) (*FetchGCSObjectProcessorContext, error) {
	return &FetchGCSObjectProcessorContext{
		ProcessorContext: pctx,
		Bucket:           pctx.Attributes.String("gcp.bucket"),
		Object:           pctx.Attributes.String("gcp.key"),
		CredentialsFile:  pctx.Attributes.String("nififix.gcp.credentialsFile"),
	}, nil
}

type FetchGCSObjectProcessor struct{}

// Process retrieves the object and streams it to pcts.Out.
func (p *FetchGCSObjectProcessor) Process(ctx context.Context, pctx *nififix.ProcessorContext) error {
	// Get my context.
	myctx, err := NewFetchGCSObjectProcessorContext(pctx)
	if err != nil {
		return err
	}
	return p.Do(ctx, myctx)
}

func (p *FetchGCSObjectProcessor) Do(ctx context.Context, pctx *FetchGCSObjectProcessorContext) error {
	// Prepare the GCSContext from the attributes.
	gcpCtx := &GCPContext{
		CredentialsFile: pctx.CredentialsFile,
	}

	// Create the gcs client.
	client, err := gcpCtx.NewStorageClient(ctx)
	if err != nil {
		return err
	}
	defer client.Close()

	// Open the object for reading.
	reader, err := client.Bucket(pctx.Bucket).Object(pctx.Object).NewReader(ctx)
	if err != nil {
		return err
	}
	defer reader.Close()

	// Copy the content of the object to pctx.Out
	_, err = io.Copy(pctx.Out, reader)
	if err != nil {
		return err
	}
	return nil
}

