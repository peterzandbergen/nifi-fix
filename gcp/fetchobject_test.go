package gcp

import (
	"bytes"
	"context"
	"nififix"
	"testing"

	"github.com/go-logr/logr"
)

func TestGetObject(t *testing.T) {
	in := bytes.Buffer{}
	out := bytes.Buffer{}
	fctx := &FetchGCSObjectProcessorContext{
		ProcessorContext: &nififix.ProcessorContext{
			In:     &in,
			Out:    &out,
			Logger: logr.Discard(),
		},
		Bucket:          "magweg",
		Object:          "upload download mappings.xlsx",
		CredentialsFile: "../secrets/blackops-upload-download-d6aac693fc6b.json",
	}
	proc := &FetchGCSObjectProcessor{}
	err := proc.Do(context.Background(), fctx)
	if err != nil {
		t.Fatalf("error getting object: %s", err)
	}
	if out.Len() == 0 {
		t.Fatalf("out is empty")
	}
}
func TestGetWithContext(t *testing.T) {
	in := bytes.Buffer{}
	out := bytes.Buffer{}
	pc := &nififix.ProcessorContext{
		In:     &in,
		Out:    &out,
		Logger: logr.Discard(),
	}
	fctx, err := NewFetchGCSObjectProcessorContext(pc)
	if err != nil {
		t.Fatalf("error creating processor context: %s", err)
	}

	fctx.Bucket = "magweg"
	fctx.Object = "upload download mappings.xlsx"
	fctx.CredentialsFile = "../secrets/blackops-upload-download-d6aac693fc6b.json"

	proc := &FetchGCSObjectProcessor{}
	err = proc.Do(context.Background(), fctx)
	if err != nil {
		t.Fatalf("error getting object: %s", err)
	}
	if out.Len() == 0 {
		t.Fatalf("out is empty")
	}
}
