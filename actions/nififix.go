package actions

import (
	"sort"

	"github.com/urfave/cli/v2"
)

func NewApp() *cli.App {
	ga := &GlobalAction{}
	app := &cli.App{
		Name: "nififix",

		Commands: []*cli.Command{
			NewFetchGCSObjectCommand(ga),
		},
	}
	sort.Sort(cli.CommandsByName(app.Commands))
	return app
}
