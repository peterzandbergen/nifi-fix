package actions

import "testing"

func TestRunApp(t *testing.T) {
	app := NewApp()
	app.Run([]string{
		"nififix",
		"fetch-gcs-object",
		"--bucket-attr=bucket",
		"--object-attr=object",
		"--out=/tmp/test-run-app.out",
		"--credentials-file=../secrets/blackops-upload-download-d6aac693fc6b.json",
		"--attributes", `{
			"bucket":"magweg",
			"object": "upload download mappings.xlsx"
		}`,
	})
}
