package actions

import (
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/urfave/cli/v2"
)

type GlobalAction struct {
	Attributes string
	In         string
	Out        string
}

func (a *GlobalAction) Flags() []cli.Flag {
	res := []cli.Flag{
		&cli.StringFlag{
			Name:        "attributes",
			Destination: &a.Attributes,
			Usage:       "nifi attributes in JSON format. Read from file when value starts with @.",
			Required:    true,
		},
		&cli.StringFlag{
			Name:        "in",
			Destination: &a.In,
			Usage:       "name of the input file, default is Stdin",
			Required:    false,
		},
		&cli.StringFlag{
			Name:        "out",
			Destination: &a.Out,
			Usage:       "name of the output file, default is Stdout",
			Required:    false,
		},
	}
	return res
}

func (a *GlobalAction) Action(*cli.Context) error {
	// Fetch the atrributes from file.
	if strings.Index(a.Attributes, "@") == 0 {
		b, err := ioutil.ReadFile(a.Attributes)
		if err != nil {
			return fmt.Errorf("cannot read attributes from file: %w", err)
		}
		a.Attributes = string(b)
	}
	return nil
}
