package actions

import (
	"context"
	"fmt"
	"nififix"
	"nififix/gcp"
	"os"
	"sort"

	"github.com/urfave/cli/v2"
)

type FetchGCSObjectAction struct {
	*GlobalAction

	CredentialsFile string

	// XxxParam allows the caller to specify the name of the attribute in the
	// Attributes JSON that contains the specified value.
	BucketAttr          string
	ObjectAttr          string
	CredentialsFileAttr string

	// parameters
	bucket          string
	object          string
	credentialsFile string
}

func runProcess(attrParam string, proc nififix.Processor) error {
	attrs, err := nififix.PrepareAttributes([]byte(attrParam))
	if err != nil {
		return err
	}

	pctx := &nififix.ProcessorContext{
		In:         os.Stdin,
		Out:        os.Stdout,
		Attributes: attrs,
	}
	return proc.Process(context.Background(), pctx)
}

func NewFetchGCSObjectCommand(ga *GlobalAction) *cli.Command {
	var flags []cli.Flag
	a := &FetchGCSObjectAction{
		GlobalAction: ga,
	}

	flags = []cli.Flag{
		&cli.StringFlag{
			Name:        "credentials-file",
			Destination: &a.CredentialsFile,
			Usage:       "GCP SA credentials file",
			DefaultText: "Can also be set in the attributes.",
			Required:    false,
		},
		&cli.StringFlag{
			Name:        "bucket-attr",
			Destination: &a.BucketAttr,
			Usage:       "attribute name in the JSON that holds the bucket name",
			Value:       "gcs.bucket",
			Required:    false,
		},
		&cli.StringFlag{
			Name:        "object-attr",
			Destination: &a.ObjectAttr,
			Usage:       "attribute name in the JSON that holds the object name",
			Value:       "gcs.key",
			Required:    false,
		},
		&cli.StringFlag{
			Name:        "credentials-file-attr",
			Destination: &a.ObjectAttr,
			Usage:       "attribute name in the JSON that holds the name of the gcp credentials file",
			Required:    false,
		},
	}

	cmd := &cli.Command{
		Name:        "fetch-gcs-object",
		Description: "Fetches an object from the bucket",
		Usage:       "Fetches an object from bucket",
		Action:      a.Action,
		Flags:       append(ga.Flags(), flags...),
	}
	sort.Sort(cli.FlagsByName(cmd.Flags))
	return cmd
}

func (a *FetchGCSObjectAction) setParameters(attrs nififix.Attributes) error {
	bucket := attrs.String(a.BucketAttr)
	if bucket == "" {
		return fmt.Errorf("bucket attribute is missing")
	}

	object := attrs.String(a.ObjectAttr)
	if object == "" {
		return fmt.Errorf("object attribute is missing")
	}

	credFile := a.CredentialsFile
	if credFile == "" {
		credFile := attrs.String(a.CredentialsFileAttr)
		if credFile == "" {
			return fmt.Errorf("credentials file attribute is missing")
		}
	}
	a.bucket = bucket
	a.object = object
	a.credentialsFile = credFile
	return nil
}

func (a *FetchGCSObjectAction) Action(appCtx *cli.Context) error {
	if err := a.GlobalAction.Action(appCtx); err != nil {
		return err
	}

	pc, err := nififix.NewProcessorContext([]byte(a.Attributes))
	if err != nil {
		return err
	}

	// set the parameters.
	if err := a.setParameters(pc.Attributes); err != nil {
		return err
	}

	fpc := &gcp.FetchGCSObjectProcessorContext{
		ProcessorContext: pc,
		Bucket:           a.bucket,
		Object:           a.object,
		CredentialsFile:  a.credentialsFile,
	}

	// Prepare the main processor context.
	if a.In != "" {
		// Open the input file.
		f, err := os.Open(a.In)
		if err != nil {
			return err
		}
		defer f.Close()
		fpc.In = f
	}

	if a.Out != "" {
		// Create the output file.
		f, err := os.Create(a.Out)
		if err != nil {
			return err
		}
		defer f.Close()
		fpc.Out = f
	}

	// Call the processor.
	proc := &gcp.FetchGCSObjectProcessor{}
	return proc.Do(appCtx.Context, fpc)
}
