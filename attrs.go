package nififix

type Attributes map[string]interface{}

func (a Attributes) String(key string) string {
	v := a[key]
	if v == nil {
		return ""
	}
	if vt, ok := v.(string); ok {
		return vt
	}
	return ""
}

func (a Attributes) Int(key string) int {
	v := a[key]
	if v == nil {
		return 0
	}
	if vt, ok := v.(int); ok {
		return vt
	}
	return 0
}
