package nififix

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"os"

	"github.com/go-logr/logr"
)

type ProcessorContext struct {
	In  io.Reader
	Out io.Writer

	Logger logr.Logger

	Attributes Attributes
}

type Processor interface {
	Process(ctx context.Context, pctx *ProcessorContext) error
}

func PrepareAttributes(d []byte) (Attributes, error) {
	var attr Attributes

	if err := json.Unmarshal(d, &attr); err != nil {
		return nil, fmt.Errorf("error unmarshalling attributes: %w", err)
	}
	return attr, nil
}

func NewProcessorContext(d []byte) (*ProcessorContext, error) {
	attrs, err := PrepareAttributes(d)
	if err != nil {
		return nil, err
	}
	return &ProcessorContext{
		In: os.Stdin,
		Out: os.Stdout,
		Logger: logr.Discard(),
		Attributes: attrs,
	}, nil
}